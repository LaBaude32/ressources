# Préparer son application

### Préparer les metadatas de votre projet

> Documentation relative aux metadonnées : [Descriptions, graphiques et captures d'écrans | F-Droid-](https://f-droid.org/fr/docs/All_About_Descriptions_Graphics_and_Screenshots/)

> Metadatas de l'application F-Droid : [metadata/en-US · master · F-Droid / Client · GitLab](https://gitlab.com/fdroid/fdroidclient/-/tree/master/metadata/en-US)

Il vous faut donc ajouter 2 langues et pour chaque :Nom

- Nom
  
- Icône
  
- Résumé
  
- Description
  
- Licence (que je n'ai personnellement pas mises parce qu'elle est dans mon fichier metadata)
  
- une entrée Nouveautés pour au moins une version
  
- au moins un élément graphique (capture d’écran ou fonctionnalité visuelle)
  
- au moins un des éléments au dessus traduit
  

Ajouter le header de la licence dans votre Readme ou dans les fichiers sources [GNU General Public License v3.0 or later | Software Package Data Exchange (SPDX)](https://spdx.org/licenses/GPL-3.0-or-later.html#licenseHeader)

### Builder votre application

1. Builder une application signé sur Android Studio - [Documentation](https://developer.android.com/studio/publish/app-signing?hl=fr#sign_release) Google
  
2. Récuperer la signature - [Documentation](https://f-droid.org/docs/Build_Metadata_Reference/#AllowedAPKSigningKeys) Fdroid - (Vous aurez besoin de `apksigner`, Sur debian installer avec : `sudo apt install apksigner`)
  
3. Envoyer l'application sur le pakage registery de votre projet gitlab [Documentation Gitlab](https://docs.gitlab.com/ee/user/packages/generic_packages/index.html)
  
  ```shell
  curl --header "PRIVATE-TOKEN: <your_access_token>" \
  --upload-file path/to/file.txt \
      "https://gitlab.example.com/api/v4/projects/<project_number>/packages/generic/my_package/<tag_version>/file.apk"
  ```
  

# Préparer une release gitlab

1. Créez un tag (ayant le même nom que votre version d'application android : `VersionName` du `build.gradle`)
  
2. Créez une release à partir de ce tag
  
3. Attacher le binaire de l'application hégergé sur le package registery dans la release : [Documentation Gitlab](https://docs.gitlab.com/ee/api/releases/links.html#create-a-release-link)
  
  Vous allez ainsi créer un lien stable pour le build de fdroid et permettant ainsi de faire un `reproductible build` [Documentation Fdroid](https://gitlab.com/fdroid/wiki/-/wikis/How-to-create-a-stable-link-for-GitLab-release-assets)
  
  Ici le `--data url` est le lien que vous trouverez dans le pakage registery utilisé avant

  Exemple : 
  ```shell
    curl --request POST \
         --data name="BoB-app-1.2.apk" \
         --data url="https://gitlab.com/LaBaude32/bob/-/package_files/89650406/download" \
         --data filepath="/app-release.apk" \
         --header "PRIVATE-TOKEN: <private_token>" \
         "https://gitlab.com/api/v4/projects/44232534/releases/1.2/assets/links"
  
  ```

# Préparer les metadatas du dépot fdroid

Article de [Ref](https://dev.to/sanandmv7/how-to-publish-your-apps-on-f-droid-2epn)

1. Installer fdroidserver :
  
  ```shell
  #mettez vous dans le dossier de votre choix
  #clonnez fdroid server depuis les sources, donne accès à la dernière version sans les problème de celle de Debian
  git clone https://gitlab.com/fdroid/fdroidserver.git
  
  #exporter la variable pour avoir accès à la commande fdroidserver
  export PATH="$PATH:$PWD/fdroidserver" 
  ```
  
2. Installer les différents paquets python manquant <u>**(ne pas utiliser pip sur debian - et autre probablement aussi -)**</u> :
  
  ```shell
  sudo apt install python3-git
  sudo apt install python3-yaml
  sudo apt install python3-defusedxml 
  sudo apt install python3-pyasn1
  sudo apt install python3-pyasn1-modules 
  sudo apt install python3-ruamel.yaml
  sudo apt install python3-qrcode
  ```
  
3. faites un fork du projet [F-Droid / Data · GitLab](https://gitlab.com/fdroid/fdroiddata)
  
4. Clonnez localement le projet sur votre machine
  
  ```shell
  git clone https://gitlab.com/<your-username>/<your-fdroid-fork>
  cd fdroiddata
  ```
  
5. Verifier que tout fonctionne :
  
  ```shell
  fdroid init
  fdroid readmeta
  ```
  
6. Importez votre projet (pour générer les metadatas)
  
  ```shell
  fdroid import --url https://github.com/YOUR-USERNAME/REPO --subdir app
  ```
  
7. Modifier le fichier `yml` créé par l'outil fdroid.
  Vous pouvez le faire soit avec nano :
  
  ```
  nano metadata/YOUR-APP-ID.yml 
  ```
  
  Soit avec un outil comme [vscodium](https://vscodium.com/) (un build de vscode sans télémétrie et sans toute les cochoneries de Micro$oft)
  
  ```shell
  codium metadata/YOUR-APP-ID.yml 
  ```
  
  > Liste de toutes les champs utilisable dans le fichier : [Compiler les références des métadonnées | F-Droid](https://f-droid.org/docs/Build_Metadata_Reference/)
  
  1. Ajouter une `License :`
    
  2. Ajouter une [catégories](https://f-droid.org/docs/Build_Metadata_Reference/#Categories)
    
  3. Ajouter un `AutoName :`
    
  4. Ajouter un `Binaries :` pour activer les [Reproducible Builds](https://f-droid.org/docs/Reproducible_Builds/) : [Binaries | F-Droid](https://f-droid.org/docs/Build_Metadata_Reference/#Binaries)
    
    1. Recuperer la signature de l'appication (vous aurez peut être besoin d'installer apksigner : `sudo apt install apksigner`)
      
    2. Ajouter la signature à votre fichier metadata : [AllowedAPKSigningKeys| F-Droid](https://f-droid.org/docs/Build_Metadata_Reference/#AllowedAPKSigningKeys)
      
  5. Ajouter les infos de version et de mise à jour :
    
    ```yml
    AutoUpdateMode: Version #Pour la mise à jour automatique sur les version
    UpdateCheckMode: Tags #Choix du syteme de mise à jour
    CurrentVersion: 1.0-rc1 #Nom de la version identique à votre build.gradle
    CurrentVersionCode: 1 #Code de la version identique à votre build.gradle
    ```
    
  6. Si dans votrer `build.gradle` il y a :
    
    ```json
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_17
        targetCompatibility JavaVersion.VERSION_17
    }
    ```
    
    Cela signifie que vous utilisez JDK (java) version 17, donc vous devez ajouter :
    
    ```yml
    #Builds:
    #  - versionName: 1.0-rc1
    #    versionCode: 1
    #    commit: 11fa289f2c4aa18b64d22b718b873199b8caf158
    #    subdir: app
    sudo:
      - apt-get update
      - apt-get install -y openjdk-17-jdk-headless
      - update-alternatives --auto java
    #    gradle:
    #      - yes
    ```
    
8. Verifiez le fichier
  
  ```shell
  fdroid readmeta
  ```
  
9. Lancer l'outil de correction du fichier
  
  ```shell
  fdroid rewritemeta YOUR-APP-ID
  ```
  
10. Tentez de linter le fichier
  
  ```shell
  fdroid lint YOUR-APP-ID
  ```
  

       Il arrive assez souvent que le lint ne fonctionne pas mais ce n'est pas gravissime

11. Tentez de builder votrer application
  
  ```shell
  fdroid build -v -l YOUR-APP-ID
  ```
  

        Si ça ne fonctionne pas il se peut que le chemin de votre SDK ne soit pas correctement renseigné (dépend de l'installation de Android Studio). Si c'est le cas vous pouvez le trouver avec AndroidStudio dans : `File > Project Structure > SDK Location`

        Ensuite faites un :

```shell
export ANDROID_HOME=$HOME/Android/Sdk (ou le chemin que vous avez trouvez sur AndroidStudio)
export PATH=$PATH:$ANDROID_HOME/tools
```

12. Envoyez votre code sur gitlab
  
  ```shell
  git add .
  git commit -m "Added <Your App Name Here>"
  git remote set-url origin https://gitlab.com/YOUR-USERNAME/fdroiddata.git
  git push -u origin master
  ```
  
13. Une pipeline va se déclancher et il va falloir faire en sorte de regler les problèmes.
  
14. Une fois que vos problèmes sont réglés vous pouvez faire une `merge request` depuis votre branche vers le projet Fdroid
